import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CustomEvents } from 'src/app/core/constants/custom-events.constant';
import { UserModel, Roles } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { EventsService } from 'src/app/core/services/events.service';
import { UserDataService } from 'src/app/core/services/user-data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  pageTitle = '';
  navbarCollapsed = true;
  user: UserModel | null = null;
  photoURL: string = 'assets/images/user-placeholder.png';
  firstName = 'Anônimo';
  roles: Roles = {
    admin: false,
    client: true,
  };
  displayName: string = "";

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.navbarCollapsed = true;
    }
  }
  
  constructor(
    private router: Router,
    private auth: AuthService,
    private userData: UserDataService,
    private eventsService: EventsService,
    private eRef: ElementRef
  ) {}

  async signOut() {
    this.user = null
    this.navbarCollapsed = false
    await this.auth.signOut();
    // this.cartService.clear()
    this.router.navigate(['/campanhas']);
  }

  goTo(path: string) {
    this.navbarCollapsed = false
    this.router.navigate([path]);
  }

  ngOnInit(): void {
    this.addUser();

    // Escuta pra ver se o usuário se logou. Sim é uma merda ter que usar dois Observers, depois refatoro.
    this.eventsService.on(CustomEvents.AUTH_CHANGE, () => {
      this.addUser();
    })

    // Verifica os eventos quando a rota muda, sem atualizar o HTML em si
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.navbarCollapsed = true;
        return
      }

      return
    })
  }

  private addUser() {
    this.user = this.userData.get();
    if (this.user) {
      this.photoURL = this.user.photoURL;
      if (this.user.displayName)
        this.displayName = this.user.displayName;
    } else {
      this.user = null;
    }
  }
}
