import { Component, OnInit, Input } from '@angular/core';
import { ResolveStart, Router, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sub-bar',
  templateUrl: './sub-bar.component.html',
  styleUrls: ['./sub-bar.component.scss']
})
export class SubBarComponent implements OnInit {

  @Input() pageTitle: string = '';
  @Input() showBar: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.route.url.subscribe(() => {
      if (this.route.snapshot.firstChild) {
        this.pageTitle = this.route.snapshot.firstChild.data['title']
      } else {
        this.pageTitle = this.route.snapshot.data['title']
      }
     });

    // Verifica os eventos quando a rota muda, sem atualizar o HTML em si
    this.router.events
      .pipe(
        filter((event) => event instanceof ResolveStart),
        map((event: any) => {
          let data = null;
          let route = event['state'].root;

          while (route) {
            data = route.data || data;
            route = route.firstChild;
          }

          return data;
        })
      )
      .subscribe((data) => {
        if (data) {
          this.pageTitle = data.title;
        }
      });
  }

  goBack() {
    this.location.back()
  }

  public get width() {
    return window.innerWidth;
  }

}
