import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { LoaderComponent } from './loader/loader.component';
import { SubBarComponent } from './sub-bar/sub-bar.component';
import { NoResultsComponent } from './no-results/no-results.component';
import { LoaderSectionComponent } from './loader-section/loader-section.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    LoaderComponent,
    SubBarComponent,
    NoResultsComponent,
    LoaderSectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbCollapseModule
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    LoaderComponent,
    SubBarComponent,
    NoResultsComponent,
    LoaderSectionComponent
  ]
})
export class LayoutModule { }
