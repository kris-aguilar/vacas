import { Pipe, PipeTransform } from '@angular/core';
import { CampaignModel } from '../models/campaign.model';

@Pipe({
  name: 'listFilter'
})
export class ListFilterPipe implements PipeTransform {

  transform(items: any, filter: any): any {
    if (!filter || !Array.isArray(items)) {
      return items;
    }
    
    if (filter && Array.isArray(items)) {
      let filterKeys = Object.keys(filter);

      return items.filter(item => true);
    }
  }
}