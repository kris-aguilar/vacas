export interface CobrancaResponseModel {
  qrCode: String,
  qrCodeImage: String,
  qrId: Number,
  expiracao: Number,
  txId: String,
}