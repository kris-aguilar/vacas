export enum CampaignStatus {
    ACTIVE = 'ACTIVE',
    CLOSED = 'CLOSED',
    SUSPENDED = 'SUSPENDED',
}