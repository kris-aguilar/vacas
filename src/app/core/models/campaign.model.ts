import { CampaignStatus } from "./campaign-status.enum";
import { Timestamp } from "@firebase/firestore-types";

export interface CampaignModel {
    id: string
    uid: string
    displayName: string
    photoURL: string
    title: string
    urlTitle: string
    image: string
    disabled: boolean
    goal: number
    revenue: number
    createDate: Timestamp
    endDate: Timestamp
    description: string
    status: CampaignStatus 
    ownerUid: string
}