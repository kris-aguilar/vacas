import { Timestamp } from "@firebase/firestore-types";
import { CobrancaResponseModel } from "./cobranca.model";
import { DonationStatus } from "./donation-status.enum";
import { PaymentMethod } from "./payment-method.enum";

export interface ProductModel {
    id: string
    campaignId: number
    price: number
    name: string
    image: string
    disabled: boolean
    stock: number
    currentStock: number
}

export interface ProductCartModel extends ProductModel {
    quantity: number
}

export interface CartModel {
    products: ProductCartModel[]
}

export interface DonationModel extends CartModel, CobrancaResponseModel {
    paymentStatus: DonationStatus
    paymentMethod: PaymentMethod
    totalPrice: number
    createDate: Timestamp
    id: string //parseInt(Math.ceil(Math.random() * Date.now()).toPrecision(16).toString().replace(".", ""))
    email: string
    campaignId: string
    campaignTitle: string
    campaignImage: string
    displayName: string
    campaignOwner: string
    uid: string
}