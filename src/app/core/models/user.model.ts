// import { Timestamp } from "@firebase/firestore-types";
import { User } from "@firebase/auth-types";

export class Roles {
  client = false;
  admin = false;
}

// export class AddressModel {
//   cep: string
//   bairro: string
//   logradouro: string
//   numero: number
//   complemento?: string
//   localidade: string
//   uf: string
// }

export interface UserModel { 
  email: string;
  firstName: string;
  lastName: string;
  uid: string;
  displayName: string;
  photoURL: string;
  roles: Roles;
  // birthDate?: Timestamp;
  // gender?: string;
  // address?: AddressModel
}