import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResumirPipe } from './pipes/resumir.pipe';
import { ListFilterPipe } from './pipes/list-filter.pipe';



@NgModule({
  declarations: [
    ResumirPipe,
    ListFilterPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ResumirPipe,
    ListFilterPipe
  ]
})
export class CoreModule { }
