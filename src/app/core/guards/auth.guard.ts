import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { tap, map, take } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { SwalService } from '../services/swal.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private swalService: SwalService
    ) { }

  canActivate(route: ActivatedRouteSnapshot, state: any): Observable<boolean> {
    return this.authService.user$.pipe(
      take(1),
      map(user => !!user), // <-- map to boolean
      tap(loggedIn => {
        if (!loggedIn) {
          this.authService.signOut()
          this.router.navigate(['/login'])
          this.swalService.errorSwal('Entre com sua conta ou registre-se para acessar esta página.', 'Você não está logado!')
        }
      })
    );
  }

}
