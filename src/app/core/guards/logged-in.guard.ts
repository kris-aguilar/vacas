import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { SwalService } from '../services/swal.service';
import { UserDataService } from '../services/user-data.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {

  constructor(
    private router: Router,
    private swalService: SwalService,
    private userData: UserDataService
  ) { }

  // Se o usuário estiver logado, não pode entrar na rota
  canActivate(): boolean {
    if (this.userData.get() != null) {
      this.swalService.errorSwal('Você já está logado!', 'Você não pode acessar esta página.')
      this.router.navigate(['/produtos'])
      return false;
    } else {
      return true;
    }
  }

}
