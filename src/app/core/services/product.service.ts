import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/compat/firestore';
import { ProductModel } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  ref: AngularFirestoreCollection = this.afStore.collection('campaigns')


  constructor(private afStore: AngularFirestore) { }

  getProducts(campaignId: string) {
    const productRef: AngularFirestoreCollection<ProductModel> = this.ref.doc(campaignId).collection('products')
    return productRef.get()
  }
}
