import { Injectable } from '@angular/core';
import { CampaignModel } from '../models/campaign.model';
import { AngularFirestore, AngularFirestoreCollection, } from '@angular/fire/compat/firestore';
import { GenericStorageService } from './generic-storage.service';
import { StorageKeys } from '../constants/storage-keys.constants';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  private ref: AngularFirestoreCollection<CampaignModel> = this.afStore.collection('campaigns')
  
  constructor(private afStore: AngularFirestore, private storage: GenericStorageService) { }
  
  set currentCampaign(campaign: CampaignModel | null) {
    campaign ? this.storage.save(StorageKeys.CURRENT_CAMPAIGN, campaign) : this.storage.remove(StorageKeys.CURRENT_CAMPAIGN)
  }

  get currentCampaign()  {
    return this.storage.get(StorageKeys.CURRENT_CAMPAIGN)
  }

  getCampaign(id: string) {
    return this.ref.doc(id).get()
  }

  getCampaigns() {
    return this.ref.get()
  }

  get userCampaigns() {
    return this.afStore.collection<CampaignModel>('campaigns', ref => ref.where('uid', '==', this.storage.get(StorageKeys.USER_DATA).uid)).get()
  }
}
