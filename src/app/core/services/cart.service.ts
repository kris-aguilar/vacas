import { Injectable } from '@angular/core';
import { GenericStorageService } from './generic-storage.service';
import { StorageKeys } from '../constants/storage-keys.constants';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProductCartModel } from '../models/product.model';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cart$: BehaviorSubject<ProductCartModel[]> = new BehaviorSubject<ProductCartModel[]>([]);

  public readonly cart: Observable<ProductCartModel[]> = this.cart$.asObservable();

  constructor(
    private localStorage: GenericStorageService,
    private toastrService: ToastrService
  ) {
    let currentCart = this.get()
    if (currentCart) {
      if (currentCart.length > 0) this.cart$.next(currentCart)
    }
  }

  get(): ProductCartModel[] {
    return this.localStorage.get(StorageKeys.SHOPPING_CART)
  }

  push(product: ProductCartModel) {
    let cart = this.cart$.getValue()
    const sameproductIndex = cart.findIndex(x => x.name === product.name)
    if (sameproductIndex !== -1) {
        if (cart[sameproductIndex].quantity >= cart[sameproductIndex].currentStock) {
          this.toastrService.error('Não há mais estoque', 'Não foi possível adicionar o produto', {closeButton: true, timeOut: 2000})
        } else {
          cart[sameproductIndex].quantity += product.quantity
          this.toastrService.success('', 'Produto adicionado ao carrinho!', {closeButton: true, timeOut: 2000})
        }
    } else {
      cart.push(product)
    }
    this.set(cart)
    this.cart$.next(cart)
  }

  remove(productId: number) {
    let cart = this.cart$.getValue()
    cart.splice(productId, 1)
    this.set(cart)
    this.cart$.next(cart)
  }

  increment(productId: number) {
    let cart = this.cart$.getValue()
    if (cart[productId].quantity >= cart[productId].currentStock) return
    cart[productId].quantity += 1
    this.set(cart)
    this.cart$.next(cart)
  }

  decrement(productId: number) {
    let cart = this.cart$.getValue()
    if (cart[productId].quantity === 1)
      return
    cart[productId].quantity -= 1
    this.set(cart)
    this.cart$.next(cart)
  }

  private set(product: ProductCartModel[]) {
    return this.localStorage.save(StorageKeys.SHOPPING_CART, product)
  }

  clear() {
    this.set([])
    this.cart$.next([])
  }
}
