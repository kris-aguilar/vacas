import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/compat/auth'
import { Observable, of } from 'rxjs'
import { UserModel, Roles } from '../models/user.model'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore'
import { User } from '@firebase/auth-types'
import { switchMap } from 'rxjs/operators'
import { UserDataService } from './user-data.service'
import { keys } from 'ts-transformer-keys';
import auth from 'firebase/compat/app';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$: Observable<UserModel | null | undefined>

  constructor(
    private afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private userData: UserDataService
  ) {
    this.user$ = afAuth.authState.pipe(
      switchMap((user) => {
        if (user) {
          return firestore.doc<UserModel>(`users/${user.uid}`).valueChanges()
        } else {
          return of(null)
        }
      })
    )
  }
  
  currentUser() {
    return this.afAuth.currentUser
  }

  async emailSignUp({ email, password, firstName, lastName }: { email: string; password: string; firstName: string; lastName: string }) {
    return this.afAuth.createUserWithEmailAndPassword(email, password).then(async (credential) => {
      await this.sendVerificationEmail()
      if (credential.user) {
        let user: UserModel = {
          firstName: firstName,
          lastName: lastName,
          displayName: `${firstName} ${lastName}`,
          photoURL: 'assets/images/user-placeholder.png',
          roles: { client: true, admin: false },
          uid: credential.user.uid,
          email: email,
        }
        this.updateUserData(user, true)
      }
    }).catch((error) => {
      throw error;
    })
  }

  async emailSignIn(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(
      email,
      password
    ).then(async (credential) => {
      if (credential.user?.emailVerified !== true) {
        throw new Error('Você ainda não verificou seu email.')
      } else {
        return credential
      }
    })
  }

  async linkGoogleAccount() {
    const provider = new auth.auth.GoogleAuthProvider()
    const user = await this.afAuth.currentUser
    return user?.linkWithPopup(provider).then((credential) => {
      // if (credential.user) this.updateUserData(credential.user, credential.additionalUserInfo?.isNewUser)
    }).catch((error) => {
      throw error
    })
  }

  async unlinkGoogleAccount() {
    const provider = new auth.auth.GoogleAuthProvider()
    const user = await this.afAuth.currentUser
    return user?.unlink(provider.providerId)
  }

  async googleSignIn() {
    const provider = new auth.auth.GoogleAuthProvider()
    const credential = await this.afAuth.signInWithPopup(provider)
      if (credential.user) {
          this.updateUserData(credential.user, false)
      }
  }

  async facebookSignIn() {

  }

  async linkCredential(credential: auth.auth.AuthCredential) {
    return this.afAuth.currentUser.then((user) => {
      user?.linkWithCredential(credential)
    }).catch((error) => {
      throw error
    })
  }

  async sendVerificationEmail() {
    const user = await this.afAuth.currentUser
    return await user?.sendEmailVerification()
  }

  async signOut() {
    this.userData.remove()
    await this.afAuth.signOut()
  }

  async getCurrentUser() {
    return await this.afAuth.currentUser
  }

  async getUser(uid: string) {
    const userRef: AngularFirestoreDocument<UserModel> = this.firestore.doc(
      `users/${uid}`
    )

    userRef.get().subscribe((user) => {
      this.userData.save(user.data() as UserModel)
    })

  }
  // Desestruturando o objeto para designar automaticamente os valores
  updateUserData(user: UserModel | User, isUserModel: boolean) {
    const userRef: AngularFirestoreDocument = this.firestore.doc(
      `users/${user.uid}`
    )
    
    if (isUserModel) { // Checa se é User ou Usermodel
      const data: UserModel = user as UserModel
      this.userData.save(data)
      return userRef.set(data, { merge: true })
    } else {
        const name = user.displayName?.split(' ', 2)
        // Eu já faço o if lá em cima, então eu tenho certeza que é userModel
        const data: UserModel = {
          firstName: name ? name[0] : '',
          lastName: name ? name[1]: '',
          displayName: user.displayName ? user.displayName : '',
          photoURL: user.photoURL ? user.photoURL : 'assets/images/user-placeholder.png',
          roles: { client: true, admin: false },
          uid: user.uid,
          email: user.email ? user.email : '',
        }

        this.userData.save(data)
        return userRef.set(data, { merge: true })
    }
  }

  async updateProfile(profile: { displayName?: string; photoURL?: string }) {
    this.afAuth.currentUser.then((user) => {
      return user?.updateProfile(profile)
    }).catch((error) => {
      throw error
    })
  }

  async resetPassword(email: string) {
    return await this.afAuth.sendPasswordResetEmail(email)
  }

  async getEmailPassCred(email: string, password: string) {
    return auth.auth.EmailAuthProvider.credential(email, password)
  }

  async getSignInMethods(email: string) {
    return await this.afAuth.fetchSignInMethodsForEmail(email)
  }

  isEmailUnique(email: string) {
    return this.firestore.collection('users', ref => ref.where('email', '==', email)).get()
  }

  private checkAuthorization(user: UserModel, allowedRoles: Roles): boolean {
    if (!user) return false

    // CARALHOOO. Tá basicamente não dava pra percorrer
    const keysofRoles = keys<Roles>();
    for (let role of keysofRoles) {
      if (user.roles[role] == allowedRoles[role]) {
        return true
      }
    }
    return false
  }
}
