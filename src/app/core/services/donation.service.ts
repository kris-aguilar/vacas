import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/compat/firestore';
import { DonationModel } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class DonationService {
  private userRef: AngularFirestoreCollection<DonationModel> = this.afStore.collection('users')

  constructor(
    private afStore: AngularFirestore
    ) { }

  async createDonation(donation: DonationModel, uid: string) {
    return this.userRef.doc(uid).collection('donations').doc(donation.id).set(donation)
  }

  getUserDonations(uid: string) {
    return this.userRef.doc(uid).collection('donations').valueChanges()
  }

  getCampaignDonations(campaignId: string) {
    const campaignDonoRef = this.afStore.collectionGroup<DonationModel>('donations', ref => ref.where('campaignId', '==', campaignId))
    return campaignDonoRef
  }

}
