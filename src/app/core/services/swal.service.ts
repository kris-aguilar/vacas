import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor(private router: Router) { }

  successSwal(message: string) {
    return Swal.fire({
      title: message,
      icon: 'success',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }

  loadingSwal(title?: string) {
    if (!title) title = 'Carregando...'
    Swal.fire({
      title: title,
      text: 'Por favor aguarde',
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      }
    });
  }

  errorSwal(message: string, title?: string) {
    if (!title) title = 'Opa!'
    return Swal.fire({
      title: title,
      text: message,
      icon: 'error',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true
    });
  }

  errorLoginSwal() {
    return Swal.fire({
      title: 'Você precisa estar logado para finalizar seu pedido!',
      text: '',
      icon: 'error',
      showCloseButton: true,
      showConfirmButton: true,
      confirmButtonText: 'Criar conta',
      confirmButtonColor: '#9c27b0',
      timerProgressBar: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['/cadastro'])
      }
    });
  }

  async warningSwal(message: string, confirmText: string, title?: string) {
    if (!title) title = 'Atenção!'
    return await Swal.fire({
      title: title,
      text: message,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não obrigado',
      confirmButtonText: confirmText
    });
  }

  close() {
    Swal.close()
  }
}
