import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CobrancaResponseModel } from '../models/cobranca.model';

@Injectable({
  providedIn: 'root'
})
export class PixService {

  constructor(private http: HttpClient) { }

  criarCobranca(expiracao: Number, valor: String, email: String) {
    return this.http.post<CobrancaResponseModel>(environment.pixApi + '/cob', {
      expiracao: expiracao,
      valor: valor,
      email: email
    })
  }
}
