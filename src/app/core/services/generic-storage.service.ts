import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GenericStorageService {

    constructor() { }

    public save(storageKey: string, value : any) {
        const json = JSON.stringify(value);
        localStorage.setItem(storageKey, json);
    }

    public remove(storageKey: string) {
        localStorage.removeItem(storageKey);
    }

    public get(storageKey: string): any {
        const item = localStorage.getItem(storageKey);
        let parsed = null;
        if (item !== undefined && item != null) {
            parsed = JSON.parse(item);
        }
        return parsed;
    }
}
