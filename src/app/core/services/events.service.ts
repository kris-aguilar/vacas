import { Injectable } from '@angular/core';
import { Subject, from } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class EventsService {

    public listeners: any;
    public eventsSubject: Subject<unknown>;
    public events: any;

    constructor() {
        this.listeners = {};
        this.eventsSubject = new Subject();

        this.events = from(this.eventsSubject);

        this.events.subscribe(
            ({ name, args }: {name: any, args: any}) => {
                if (this.listeners[name]) {
                    for (const listener of this.listeners[name]) {
                        listener(...args);
                    }
                }
            });
    }

    on(name: string | number, listener: any) {
        if (!this.listeners[name]) {
            this.listeners[name] = [];
        }
        this.listeners[name].push(listener);
    }

    broadcast(name: string, ...args: undefined[]) {
        this.eventsSubject.next({
            name,
            args
        });
    }

    removeListener(name: string | number) {
        delete this.listeners[name];
    }
}
