import { Injectable } from '@angular/core';
import { CustomEvents } from '../constants/custom-events.constant';
import { StorageKeys } from '../constants/storage-keys.constants';
import { UserModel } from '../models/user.model';
import { EventsService } from './events.service';
import { GenericStorageService } from './generic-storage.service';

@Injectable({
    providedIn: 'root'
})
export class UserDataService {

    constructor(private storageData: GenericStorageService, private eventsService: EventsService) { }

    public save(data: UserModel): void {
      this.storageData.save(StorageKeys.USER_DATA, data)
      this.eventsService.broadcast(CustomEvents.AUTH_CHANGE)
    }

    public get(): UserModel {
      const user = this.storageData.get(StorageKeys.USER_DATA)
      return user
    }

    public remove(): void {
        this.storageData.remove(StorageKeys.USER_DATA)
        this.eventsService.broadcast(CustomEvents.AUTH_CHANGE)
    }
}
