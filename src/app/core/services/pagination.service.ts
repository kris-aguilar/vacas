import { Injectable } from '@angular/core';
import { QueryConfig } from '../models/query-config.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs/internal/Observable';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/compat/firestore';
import { scan } from 'rxjs/internal/operators/scan';
import { take } from 'rxjs/internal/operators/take';
import { map } from 'rxjs/operators';
import firestore from "firebase/compat/app";

@Injectable({
  providedIn: 'root',
})
export class PaginationService {

  // Source data
  private _done = new BehaviorSubject(false);
  private _loading = new BehaviorSubject(false);
  private _data = new BehaviorSubject([]);
  private _noResults = new BehaviorSubject(false);
  private hasMore = false

  private query: QueryConfig = {
    field: 'createDate',
    limit: 10,
    path: '',
    prepend: false,
    reverse: true
  }

  // Observable data
  data!: Observable<any>;
  done: Observable<boolean> = this._done.asObservable();
  loading: Observable<boolean> = this._loading.asObservable();
  noResults = this._noResults.asObservable();


  constructor(private afStore: AngularFirestore) { }

  // Initial query sets options and defines the Observable
  // passing opts will override the defaults
  init<T>(path: string, field: string,  where: {fieldPath: string, opStr: firestore.firestore.WhereFilterOp, value: string} | null, opts?: any) {
    this.query = {
      path,
      field,
      limit: 5,
      reverse: false,
      prepend: false,
    }

    if (opts['collectionGroup']) {
      console.log(opts['collectionGroup'], where)
      const first = this.afStore.collectionGroup<T>(this.query.path, ref => {
        if (where) {
          return ref.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .limit(this.query.limit).where(where.fieldPath, where.opStr, where.value)
        } else {
          return ref.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .limit(this.query.limit)
        }
      })
  
      this.mapAndUpdate(first, true)
    } else {
      const first = this.afStore.collection<T>(this.query.path, ref => {
        if (where) {
          return ref.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .limit(this.query.limit).where(where.fieldPath, where.opStr, where.value)
        } else {
          return ref.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .limit(this.query.limit)
        }
      })
  
      this.mapAndUpdate(first, true)
    }

    // Create the observable array for consumption in components
    this.data = this._data.asObservable().pipe(
      scan((acc, val) => {
        return this.query.prepend ? val.concat(acc) : acc.concat(val)
      })
    )
  }

  clear() {
    this._done.next(false)
    this._loading.next(false)
    this._data.next([])
  }

  // Retrieves additional data from firestore
  more() {
    const cursor = this.getCursor()

    const more = this.afStore.collection(this.query.path, ref => {
      return ref.orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
        .limit(this.query.limit)
        .startAfter(cursor)
    })
    this.mapAndUpdate(more, false)
  }


  // Determines the doc snapshot to paginate query
  private getCursor() {
    const current: any = this._data.value
    if (current.length) {
      return this.query.prepend ? current[0].doc : current[current.length - 1].doc
    }
    return null
  }


  // Maps the snapshot to usable format the updates source
  private mapAndUpdate(col: AngularFirestoreCollection<any> | AngularFirestoreCollectionGroup<any>, isFirst: boolean) {

    if (this._done.value || this._loading.value) { return };

    // loading
    this._loading.next(true)
    this._noResults.next(false)

    // Map snapshot with doc ref (needed for cursor)
    return col.snapshotChanges().pipe(
      take(1),
      map(arr => {
        let values: any = arr.map(snap => {
          const data = snap.payload.doc.data()
          const doc = snap.payload.doc
          return { ...data, doc }
        })

        // If prepending, reverse the batch order
        values = this.query.prepend ? values.reverse() : values

        // update source with new values, done loading
        this._data.next(values)
        this._loading.next(false)

        if (values.length === 0) {
          this._done.next(!isFirst)
          this._noResults.next(isFirst)
        }
      })
    )
      .subscribe()

  }

}
