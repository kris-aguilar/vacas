import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { SwalService } from 'src/app/core/services/swal.service';
import Swal from 'sweetalert2';
import { AuthCredential } from '@firebase/auth-types';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signupForm!: FormGroup;

  isPasswordVisible = true;
  isConfirmVisible = true;
  @ViewChild('password')
  password!: ElementRef;
  @ViewChild('confirmPassword')
  confirmPassword!: ElementRef;
  isSubmit = false;
  loading = false;
  acceptedTerms = false;

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private swalService: SwalService
  ) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group(
      {
        email: [
          '',
          Validators.required,
          /*this.emailValidator.validate.bind(this.emailValidator)*/,
        ],
        password: ['', [Validators.required, Validators.minLength(8)]],
        confirmPassword: [
          '',
          [
            Validators.required,
            Validators.minLength(8),
            this.matchValues('password'),
          ],
        ],
        firstName: ['', [Validators.required, Validators.maxLength(255)]],
        lastName: ['', [Validators.required, Validators.maxLength(255)]],
        acceptedTerms: [this.acceptedTerms, [Validators.required]],
      },
      { updateOn: 'blur' }
    );

    this.signupForm.controls['password'].valueChanges.subscribe(() => {
      this.signupForm.controls['confirmPassword'].updateValueAndValidity();
    });
  }

  loginFacebook() {

  }

  async loginGoogle() {
    try {
      this.loading = true
      this.swalService.loadingSwal('Logando usuário...')
      await this.authService.googleSignIn()
      this.swalService.close()
      this.loading = false
      this.redirect()
    } catch (error) {
      this.swalService.close()
      this.loading = false
      this.handleLoginErrors(error)
    }
  }

  toggleConfirm() {
    this.isConfirmVisible = !this.isConfirmVisible;
    if (!this.isConfirmVisible) {
      this.renderer.setAttribute(
        this.confirmPassword.nativeElement,
        'type',
        'text'
      );
    } else {
      this.renderer.setAttribute(
        this.confirmPassword.nativeElement,
        'type',
        'password'
      );
    }
  }

  togglePassword() {
    this.isPasswordVisible = !this.isPasswordVisible;
    if (!this.isPasswordVisible) {
      this.renderer.setAttribute(this.password.nativeElement, 'type', 'text');
    } else {
      this.renderer.setAttribute(
        this.password.nativeElement,
        'type',
        'password'
      );
    }
  }

  matchValues(
    matchTo: string // name of the control to match to
  ): (AbstractControl: AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === this.formControls[matchTo].value
        ? null
        : { isMatching: false };
    };
  }

  // Pra debuggar os Validators
  getFormValidationErrors() {
    Object.keys(this.signupForm.controls).forEach((key) => {
      const controlErrors: ValidationErrors | null = this.signupForm.controls['key'].errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach((keyError) => {
          console.log(
            'Key control: ' + key + ', keyError: ' + keyError + ', err value: ',
            controlErrors[keyError]
          );
        });
      }
    });
  }

  async register() {
    this.isSubmit = true;
    if (this.signupForm.invalid) {
      if (this.signupForm.controls['acceptedTerms'].hasError('required')) {
        this.toastr.clear();
        this.toastr.error(
          'Você não concordou com nossos termos!',
          'Não foi possível registrar sua conta'
        );
      }
      return;
    }

    const value = this.signupForm.value;
    this.loading = true;
    await this.authService.emailSignUp({
      email: value.email,
      password: value.password,
      firstName: value.firstName,
      lastName: value.lastName
    }).then(() => {
      this.loading = false
      this.emailSentSwal()
    }).catch((error) => {
      this.loading = false;
      this.handleLoginErrors(error)
    });
  }

  private async handleLoginErrors(error: any) {
    console.error(error)
    if (error.message == 'Você ainda não verificou seu email.') {
      const verification = await this.verificationSwal()
      if (verification.value) {
        try {
          await this.authService.sendVerificationEmail()
          this.authService.signOut()
          this.emailSentSwal();
        } catch (error: any) {
          this.authService.signOut()
          console.error(error)
          this.toastr.error(error.code, 'Não foi possível enviar o email')
        }
      } else {
        this.authService.signOut()
      }
    } else {
      switch (error.code) {
        case 'invalid-argument':
          this.toastr.error('Os dados inseridos estão incorretos', 'Não foi possivel fazer o login.')
          break
        case 'auth/user-not-found':
          this.toastr.error('Email não está registrado', 'Não foi possivel fazer o login.')
          break
        case 'auth/cancelled-popup-request':
          this.toastr.error('O popup foi fechado', 'Não foi possivel fazer o login.')
          break
        case 'auth/popup-closed-by-user':
          this.toastr.error('O popup foi fechado', 'Não foi possivel fazer o login.')
          break
        case 'auth/account-exists-with-different-credential':
          const methods = await this.authService.getSignInMethods(error.email)
          this.accountAlreadyExists(error.email, methods, error.credential)
          break
        case 'auth/wrong-password':
          const meth = await this.authService.getSignInMethods(error.email)
          if (!meth.includes('password')) {
            const credential = await this.authService.getEmailPassCred(this.signupForm.value.email, this.signupForm.value.password);
            this.accountAlreadyExists(error.email, meth, credential)
            break
          }
          this.toastr.error('Dados incorretos', 'Não foi possivel fazer o login.')
          break
        case 'auth/email-already-in-use':
          this.swalService.errorSwal('Já existe uma conta vinculada a este email', 'Não foi possível criar sua conta!');
          this.router.navigateByUrl('/login')
          break;
        default:
          this.toastr.error(error.code, 'Não foi possivel fazer o login.')
          break
      }
    }
  }

  private async accountAlreadyExists(email: string, methods: Array<string>, errorCredential: AuthCredential) {

    const linkSwal = await Swal.fire({
      title: 'Já existe uma conta vinculada a esse email!',
      text: `O endereço ${email} já está vinculado a um login
        ${this.getMethod(methods[0])}. Deseja vincular essa conta com o login ${this.getMethod(errorCredential.signInMethod)}?`,
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não obrigado',
      confirmButtonText: 'Sim, vincular conta'
    })
    if (linkSwal.value) {
      this.handleLoginMethods(methods[0], email, errorCredential)
    }
  }

  private async handleLoginMethods(method: string, email: string, errorCredential: AuthCredential) {
    switch (method) {
      case 'google.com':
        try {
          this.swalService.loadingSwal()
          await this.authService.googleSignIn()
          const user = await this.authService.getCurrentUser()
          if (user) {
            await user.linkWithCredential(errorCredential)
            Swal.close()
            this.linkCredentialSuccess(errorCredential)
          }
        } catch (error) {
          Swal.close()
          this.handleLoginErrors(error)
        }
        break;
      case 'facebook.com':
        try {
          this.swalService.loadingSwal()
          await this.authService.facebookSignIn()
          const user = await this.authService.getCurrentUser()
          if (user) {
            await user.linkWithCredential(errorCredential)
            Swal.close()
            this.linkCredentialSuccess(errorCredential)
          }
        } catch (error) {
          Swal.close()
          this.handleLoginErrors(error)
        }
        break;
      case 'password':
        this.inputPasswordSwal(email, errorCredential)
        break;
    }
  }

  private async linkCredentialSuccess(credential: AuthCredential) {
    return await Swal.fire({
      title: `Conta ${this.getMethod(credential.signInMethod)} vinculada com sucesso!`,
      icon: 'success',
      text: 'Você pode usá-la para se logar a partir de agora.',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true
    })
  }

  private getMethod(method: string): string {
    switch (method) {
      case 'google.com':
        return 'Google'
      case 'password':
        return 'email e senha'
      case 'facebook.com':
        return 'Facebook'
      default:
        return ''
    }
  }

  private async inputPasswordSwal(email: string, errorCredential: AuthCredential) {
    const input = await Swal.fire({
      title: `Insira a senha da conta ${email}`,
      input: 'password',
      inputPlaceholder: 'Senha',
      showCloseButton: true
    })

    if (input.value) {
      const password = input.value

      try {

        await this.authService.emailSignIn(email, password)
        await this.authService.linkCredential(errorCredential)
        await this.linkCredentialSuccess(errorCredential)
        this.redirect()
      } catch (error) {
        console.error(error)
        const wrongPass = await Swal.fire({
          title: 'Opa!',
          icon: 'error',
          text: 'Sua senha está incorreta.',
          confirmButtonText: 'Tentar novamente',
          showCloseButton: true,
          timer: 8000,
          timerProgressBar: true
        })
        if (wrongPass.value) this.inputPasswordSwal(email, errorCredential)
      }
    }
  }

  private verificationSwal() {
    return Swal.fire({
      title: 'Opa!',
      text: 'Você ainda não verificou o seu email.',
      icon: 'error',
      confirmButtonText: 'Enviar verificação',
      showCloseButton: true
    })
  }

  private async emailSentSwal() {
    await this.authService.signOut()
    return Swal.fire({
      title: 'Quase lá!',
      text:
        'Para concluir seu cadastro, vá até sua caixa postal e confirme seu email',
      icon: 'warning',
      showCloseButton: true,
      timer: 3000,
      timerProgressBar: true,
      didClose: async () => {
        this.router.navigate(['/login']);
      },
    })
  }

  private redirect() {
    this.router.navigate(['/campanhas']);
  }

  get formControls() {
    return this.signupForm.controls;
  }
}
