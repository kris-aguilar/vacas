import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CampaignModel } from 'src/app/core/models/campaign.model';
import { ProductModel } from 'src/app/core/models/product.model';
import { CampaignService } from 'src/app/core/services/campaign.service';
import { CartService } from 'src/app/core/services/cart.service';
import { ProductService } from 'src/app/core/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: ProductModel[] = []
  loading = true
  noResults = false
  id = ''
  sub!: Subscription

  constructor(
    private productService: ProductService, 
    private campaignService: CampaignService,
    private cartService: CartService,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: ToastrService
    ) { 

    }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(async (params: Params) => {
      this.id = params['id']
      this.productService.getProducts(this.id).forEach(products => {
        products.docs.forEach(doc => {
          this.products.push(doc.data())
        })
        this.loading = false
        this.noResults = this.products.length === 0
      }).catch(err => {
        this.loading = false
        this.toastrService.error(err.code, 'Não foi possível carregar os produtos')
      })
      if (!this.campaignService.currentCampaign) {
        this.campaignService.getCampaign(this.id).subscribe(campaign => {
          if (campaign.exists) {
            this.campaignService.currentCampaign = campaign.data() as CampaignModel
          }
        })
      }
    })
  }

  getProduct() {
    
  }

  public addToCart(product: ProductModel) {
    this.cartService.push({
      campaignId: product.campaignId,
      currentStock: product.currentStock,
      image: product.image,
      name: product.name,
      price: product.price,
      id: product.id,
      disabled: product.disabled,
      stock: product.stock,
      quantity: 1
    })
  }

  public buyNow(product: ProductModel) {
    this.addToCart(product)
    this.router.navigate(['/contribua'])
  }

}
