import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CampaignModel } from 'src/app/core/models/campaign.model';
import { CampaignService } from 'src/app/core/services/campaign.service';

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.scss']
})
export class CampaignsComponent implements OnInit {

  campaigns: CampaignModel[] = []
  noResults = true
  loading = true
  search = ''

  constructor(
    private campaignService: CampaignService, 
    private toastrService: ToastrService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.campaignService.getCampaigns().forEach(campaigns => {
      campaigns.docs.forEach(doc => {
        this.campaigns.push(doc.data())
      })
      this.loading = false
      this.noResults = this.campaigns.length === 0
    }).catch(err => {
      this.loading = false
      this.toastrService.error(err.code, 'Não foi possível carregar as campanhas')
    })
  }

  goToCampaign(campaign: CampaignModel) {
    this.router.navigate([`/campanhas/${campaign.id}`]).then(() => {
      this.campaignService.currentCampaign = campaign
    })
  }

}
