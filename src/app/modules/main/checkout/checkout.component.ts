import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { catchError, finalize, retry, Subscription, throwError } from 'rxjs';
import { DonationModel, ProductCartModel } from 'src/app/core/models/product.model';
import { UserModel } from 'src/app/core/models/user.model';
import { CartService } from 'src/app/core/services/cart.service';
import { UserDataService } from 'src/app/core/services/user-data.service';
import { PaymentMethod } from 'src/app/core/models/payment-method.enum';
import { DonationService } from 'src/app/core/services/donation.service';
import { Timestamp } from "firebase/firestore";
import { PixService } from 'src/app/core/services/pix.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CampaignService } from 'src/app/core/services/campaign.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { CobrancaResponseModel } from 'src/app/core/models/cobranca.model';
import { DonationStatus } from 'src/app/core/models/donation-status.enum';
import { SwalService } from 'src/app/core/services/swal.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit, OnDestroy {
  cartItems: ProductCartModel[] = [];
  cartSubscription: Subscription = new Subscription();
  noResults = false;
  loading = false;
  subTotal = 0;
  totalItems = 0;
  removedItem = false;
  cartForm: FormGroup;
  isSubmit = false;
  uid: string = '';
  minCart = 44;
  phonePattern = /^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$/;
  noPhone: boolean = false;
  user: UserModel | null = null;
  bairros = [
    'Adrianópolis',
    'Nossa Senhora das Graças',
  ];

  constructor(
    private cartService: CartService,
    private router: Router,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private userData: UserDataService,
    private donationService: DonationService,
    private campaignService: CampaignService,
    private authService: AuthService,
    private swalService: SwalService,
    private pixService: PixService
  ) {
    this.cartForm = this.formBuilder.group({
      paymentMethod: ['PIX', Validators.required],
    });
  }

  ngOnInit(): void {
    // window.scroll(0, 0);
    this.loading = false;

    this.cartSubscription = this.cartService.cart.subscribe((items) => {
      this.cartItems = items;
      this.noResults = items.length === 0
      this.totalItems = items.length;

      let total = 0;
      items.forEach((item) => {
        total += item.price * item.quantity;
      });
      this.subTotal = total;
    });
  }

  async submit() {
    this.isSubmit = true;
    if (this.cartForm.invalid) {
      return;
    }

    this.loading = true;
    const user = await this.authService.currentUser()
    if (user) {
      if (!user.emailVerified) {
        this.loading = false
        await this.emailError()
        return
      }
      const donation: DonationModel = {
        createDate: Timestamp.fromDate(new Date()),
        paymentStatus: DonationStatus.PENDING,
        paymentMethod: PaymentMethod.PIX,
        displayName: user.displayName ? user.displayName : '',
        email: user.email ? user.email : '',
        expiracao: 86400, // 24 horas
        id: Math.ceil(Math.random() * Date.now()).toPrecision(16).toString().replace(".", ""),
        qrCode: '', // txId qrCode e qrId serão gerados pela API
        qrId: -1,
        qrCodeImage: '',
        txId: '',
        campaignId: '', // dados de campanha adicionados abaixo
        campaignImage: 'assets/images/placeholder.png',
        campaignTitle: '',
        campaignOwner: '',
        totalPrice: this.subTotal,
        uid: this.uid,
        products: this.cartItems
      }

      if (this.campaignService.currentCampaign) {
        donation.campaignId = this.campaignService.currentCampaign.id
        donation.campaignImage = this.campaignService.currentCampaign.image
        donation.campaignTitle = this.campaignService.currentCampaign.title
      }

      this.pixService.criarCobranca(donation.expiracao, donation.totalPrice.toFixed(2), donation.email).pipe(
        retry(2),
        catchError(err => {
          this.loading = false
          this.toastr.error('Tente novamente mais tarde.', 'Ocorreu um erro ao gerar a cobrança.');
          return this.handleError(err);
        }),
        finalize(() => { })
      ).subscribe((response: CobrancaResponseModel) => {
        donation.expiracao = response.expiracao
        donation.qrCode = response.qrCode
        donation.qrCodeImage = response.qrCodeImage
        donation.qrId = response.qrId
        donation.txId = response.txId
        this.donationService.createDonation(donation, user.uid).then((value) => {
          this.toastr.success('Pedido realizado com sucesso!');
          this.loading = false;
          this.cartSubscription.unsubscribe();
          this.cartService.clear();
          this.campaignService.currentCampaign
          this.router.navigate(['/usuario/pix', { qrCode: donation.qrCode, qrCodeImage: donation.qrCodeImage, expiracao: donation.expiracao }]);
        }).catch((error) => {
          this.loading = false;
          console.error(error);
          this.toastr.error(error.message, 'Não foi possível fazer seu pedido');
        })
      })
    } else {
      this.loading = false;
      this.swalService.errorLoginSwal()
    }

  }

  private async emailError() {
    const verification = await this.verificationSwal()
    if (verification.value) {
      try {
        await this.authService.sendVerificationEmail()
        this.swalService.successSwal('Email enviado com sucesso!');
      } catch (error: any) {
        this.authService.signOut()
        console.error(error)
        this.toastr.error(error.code, 'Não foi possível enviar o email')
      }
    }
  }

  private verificationSwal() {
    return Swal.fire({
      title: 'Opa!',
      text: 'Você ainda não verificou o seu email.',
      icon: 'error',
      confirmButtonText: 'Enviar verificação',
      showCloseButton: true
    })
  }

  public handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => {
      new Error('Something bad happened; please try again later.')
    });
  }

  incrementItem(id: number) {
    this.cartService.increment(id);
  }

  decrementItem(id: number) {
    this.cartService.decrement(id);
  }

  deleteItem(id: number) {
    this.cartService.remove(id);
    this.removedItem = true;
  }

  nextDay(x: number) {
    var now = new Date();
    now.setDate(now.getDate() + ((x + (7 - now.getDay())) % 7));
    return now;
  }

  ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
  }

  get formControls() {
    return this.cartForm.controls;
  }
}
