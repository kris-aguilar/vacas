import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { SwalService } from 'src/app/core/services/swal.service';
import Swal from 'sweetalert2';
import { AuthCredential } from '@firebase/auth-types';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { UserDataService } from 'src/app/core/services/user-data.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signInForm!: FormGroup;

  isPasswordVisible = true;
  isConfirmVisible = true;
  @ViewChild('password')
  password!: ElementRef;
  isSubmit = false;
  loading = false;
  acceptedTerms = false;

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private swalService: SwalService,
    private userData: UserDataService
  ) { }

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group(
      {
        email: [
          '',
          Validators.required,
        ],
        password: ['', [Validators.required, Validators.minLength(8)]]
      },
      { updateOn: 'blur' }
    );
  }

  loginFacebook() {

  }

  async loginGoogle() {
    try {
      this.loading = true
      this.swalService.loadingSwal('Logando usuário...')
      await this.authService.googleSignIn()
      this.swalService.close()
      this.loading = false
      this.redirect()
    } catch (error) {
      this.swalService.close()
      this.loading = false
      this.handleLoginErrors(error)
    }
  }

  togglePassword() {
    this.isPasswordVisible = !this.isPasswordVisible;
    if (!this.isPasswordVisible) {
      this.renderer.setAttribute(this.password.nativeElement, 'type', 'text');
    } else {
      this.renderer.setAttribute(
        this.password.nativeElement,
        'type',
        'password'
      );
    }
  }

  // Pra debuggar os Validators
  getFormValidationErrors() {
    Object.keys(this.signInForm.controls).forEach((key) => {
      const controlErrors: ValidationErrors | null = this.signInForm.controls['key'].errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach((keyError) => {
          console.log(
            'Key control: ' + key + ', keyError: ' + keyError + ', err value: ',
            controlErrors[keyError]
          );
        });
      }
    });
  }

  async login() {
    if (this.signInForm.invalid){
      return;
    }

    this.loading = true;
    const email = this.signInForm.value.email
    const password = this.signInForm.value.password

    this.authService.emailSignIn(email, password).then((value) => {
      this.loading = false
      this.finishLogin()
    }).catch((error: any) => {
      this.loading = false;
      error['email'] = this.signInForm.value.email
      error['credential'] = {}
      error.credential['signInMethod'] = 'password'
      this.handleLoginErrors(error)
      })
  }

  private finishLogin() {
    this.authService.user$.pipe(
      distinctUntilChanged()
    ).subscribe(user => {
      if (user) {
        this.userData.save(user)
        this.loading = false
        this.redirect()
      }
    })
  }

  forgotPassword() {
    Swal.fire({
      title: 'Insira seu email para poder recuperar sua senha',
      input: 'email',
      inputPlaceholder: 'Email',
      showCloseButton: true
    }).then(res => {
      if ('value' in res) this.authService.resetPassword(res.value)
      .then(()=> {
        Swal.fire({
          title: 'Email enviado com sucesso!',
          showCloseButton: true,
          timer: 5000,
          timerProgressBar: true
        })
      })
      .catch((error: { message: string; }) => {
        let message = ''
        if (error.message === 'EMAIL_NOT_FOUND') message = '(Email não encontrado)'
        Swal.fire({
          title: 'Opa!',
          icon: 'error',
          text: 'Não foi possível enviar o email de recuperação'+ message + '.',
          showCloseButton: true,
          timer: 5000,
          timerProgressBar: true
        })
      })
    })
  }

  private async handleLoginErrors(error: any) {
    console.error(error)
    if (error.message == 'Você ainda não verificou seu email.') {
      const verification = await this.verificationSwal()
      if (verification.value) {
        try {
          await this.authService.sendVerificationEmail()
          this.authService.signOut()
          this.emailSentSwal();
        } catch (error: any) {
          this.authService.signOut()
          console.error(error)
          this.toastr.error(error.code, 'Não foi possível enviar o email')
        }
      } else {
        this.authService.signOut()
      }
    } else {
      switch (error.code) {
        case 'invalid-argument':
          this.toastr.error('Os dados inseridos estão incorretos', 'Não foi possivel fazer o login.')
          break
        case 'auth/user-not-found':
          this.toastr.error('Email não está registrado', 'Não foi possivel fazer o login.')
          break
        case 'auth/cancelled-popup-request':
          this.toastr.error('O popup foi fechado', 'Não foi possivel fazer o login.')
          break
        case 'auth/popup-closed-by-user':
          this.toastr.error('O popup foi fechado', 'Não foi possivel fazer o login.')
          break
        case 'auth/account-exists-with-different-credential':
          const methods = await this.authService.getSignInMethods(error.email)
          this.accountAlreadyExists(error.email, methods, error.credential)
          break
        case 'auth/wrong-password':
          const meth = await this.authService.getSignInMethods(error.email)
          if (!meth.includes('password')) {
            const credential = await this.authService.getEmailPassCred(this.signInForm.value.email, this.signInForm.value.password);
            this.accountAlreadyExists(error.email, meth, credential)
            break
          }
          this.toastr.error('Dados incorretos', 'Não foi possivel fazer o login.')
          break
        case 'auth/email-already-in-use':
          this.swalService.errorSwal('Já existe uma conta vinculada a este email', 'Não foi possível criar sua conta!');
          this.router.navigateByUrl('/login')
          break;
        default:
          this.toastr.error(error.code, 'Não foi possivel fazer o login.')
          break
      }
    }
  }

  private async accountAlreadyExists(email: string, methods: Array<string>, errorCredential: AuthCredential) {

    const linkSwal = await Swal.fire({
      title: 'Já existe uma conta vinculada a esse email!',
      text: `O endereço ${email} já está vinculado a um login
        ${this.getMethod(methods[0])}. Deseja vincular essa conta com o login ${this.getMethod(errorCredential.signInMethod)}?`,
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não obrigado',
      confirmButtonText: 'Sim, vincular conta'
    })
    if (linkSwal.value) {
      this.handleLoginMethods(methods[0], email, errorCredential)
    }
  }

  private async handleLoginMethods(method: string, email: string, errorCredential: AuthCredential) {
    switch (method) {
      case 'google.com':
        try {
          this.swalService.loadingSwal()
          await this.authService.googleSignIn()
          const user = await this.authService.getCurrentUser()
          if (user) {
            await user.linkWithCredential(errorCredential)
            Swal.close()
            this.linkCredentialSuccess(errorCredential)
            this.redirect()
          }
        } catch (error) {
          Swal.close()
          this.handleLoginErrors(error)
        }
        break;
      case 'facebook.com':
        try {
          this.swalService.loadingSwal()
          await this.authService.facebookSignIn()
          const user = await this.authService.getCurrentUser()
          if (user) {
            await user.linkWithCredential(errorCredential)
            Swal.close()
            this.linkCredentialSuccess(errorCredential)
            this.redirect()
          }
        } catch (error) {
          Swal.close()
          this.handleLoginErrors(error)
        }
        break;
      case 'password':
        this.inputPasswordSwal(email, errorCredential)
        break;
    }
  }

  private async linkCredentialSuccess(credential: AuthCredential) {
    return await Swal.fire({
      title: `Conta ${this.getMethod(credential.signInMethod)} vinculada com sucesso!`,
      icon: 'success',
      text: 'Você pode usá-la para se logar a partir de agora.',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true
    })
  }

  private getMethod(method: string): string {
    switch (method) {
      case 'google.com':
        return 'Google'
      case 'password':
        return 'email e senha'
      case 'facebook.com':
        return 'Facebook'
      default:
        return ''
    }
  }

  private async inputPasswordSwal(email: string, errorCredential: AuthCredential) {
    const input = await Swal.fire({
      title: `Insira a senha da conta ${email}`,
      input: 'password',
      inputPlaceholder: 'Senha',
      showCloseButton: true
    })

    if (input.value) {
      const password = input.value

      try {

        await this.authService.emailSignIn(email, password)
        await this.authService.linkCredential(errorCredential)
        await this.linkCredentialSuccess(errorCredential)
        this.redirect()
      } catch (error) {
        console.error(error)
        const wrongPass = await Swal.fire({
          title: 'Opa!',
          icon: 'error',
          text: 'Sua senha está incorreta.',
          confirmButtonText: 'Tentar novamente',
          showCloseButton: true,
          timer: 8000,
          timerProgressBar: true
        })
        if (wrongPass.value) this.inputPasswordSwal(email, errorCredential)
      }
    }
  }

  private verificationSwal() {
    return Swal.fire({
      title: 'Opa!',
      text: 'Você ainda não verificou o seu email.',
      icon: 'error',
      confirmButtonText: 'Enviar verificação',
      showCloseButton: true
    })
  }

  private emailSentSwal() {
    return Swal.fire({
      title: 'Quase lá!',
      text:
        'Para concluir seu cadastro, vá até sua caixa postal e confirme seu email',
      icon: 'warning',
      showCloseButton: true,
      timer: 3000,
      timerProgressBar: true,
      didClose: async () => {
        await this.authService.signOut();
        this.redirect();
      },
    })
  }

  private redirect() {
    this.router.navigate(['/produtos']);
  }

  get formControls() {
    return this.signInForm.controls;
  }
}
