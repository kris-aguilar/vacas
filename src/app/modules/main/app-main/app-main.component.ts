import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { UserModel } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { CartService } from 'src/app/core/services/cart.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-app-main',
  templateUrl: './app-main.component.html',
  styleUrls: ['./app-main.component.scss']
})
export class AppMainComponent implements OnInit {

  currentRoute: string = ''
  cartItems: number = 0
  isLoggedIn: boolean = false

  constructor(
    private router: Router, 
    private cartService: CartService,
    private authService: AuthService,
    ) {
  }

  ngOnInit(): void {
    this.authService.user$.subscribe(user => {
      this.isLoggedIn = user !== null
    })

    this.cartService.cart.subscribe(items => {
      this.cartItems = items.length
    })

    this.currentRoute = this.router.url

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.url
      }
    })
  }
}
