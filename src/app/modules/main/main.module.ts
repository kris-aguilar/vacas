import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { AppMainComponent } from './app-main/app-main.component';
import { CoreModule } from 'src/app/core/core.module';
import { LayoutModule } from 'src/app/layout/layout.module';
import { ProductsComponent } from './products/products.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { CheckoutComponent } from './checkout/checkout.component';


@NgModule({
  declarations: [
    AppMainComponent,
    ProductsComponent,
    SignUpComponent,
    SignInComponent,
    CampaignsComponent,
    CheckoutComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    CoreModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module
  ],
  providers: [
  ]
})
export class MainModule { }
