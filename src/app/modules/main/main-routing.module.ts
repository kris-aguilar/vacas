import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { LoggedInGuard } from 'src/app/core/guards/logged-in.guard';
import { AppMainComponent } from './app-main/app-main.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductsComponent } from './products/products.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [{
  path: '',
  data: { title: 'Início' },
  canActivate: [
  ],
  children: [
    { path: '', redirectTo: 'campanhas', pathMatch: 'full' },
    { path: '.', redirectTo: 'campanhas' },
    { path: 'cadastro', canActivate: [LoggedInGuard], component: SignUpComponent, data: { title: 'Cadastre-se' } },
    { path: 'login', canActivate: [LoggedInGuard], component: SignInComponent, data: { title: 'Login' } },
    { path: 'campanhas', component: CampaignsComponent, data: { title: 'Campanhas' } },
    { path: 'campanhas/:id', component: ProductsComponent, data: { title: 'Campanha' } },
    { path: 'contribua', component: CheckoutComponent, data: { title: 'Carrinho' } },
    {
      path: 'usuario',
      data: { title: 'Usuário'},
      canActivate: [AuthGuard],
      loadChildren: () => import('../user/user.module').then(m => m.UserModule)
    }
  ],
  component: AppMainComponent}, 
  {path: '**', redirectTo: 'campanhas'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
