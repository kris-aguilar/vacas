import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  currentRoute = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public isGroupActive(stateNames: string): boolean {
    const states = stateNames.trim().split(',');
    let isActive = false;
    this.getCurrentPartial();
    states.forEach((stateName) => {
      const route = this.currentRoute.split('/');
      if (route.length > 1) {
        if (route[0] === stateName.trim()) {
          isActive = true;
        }
      } else {
        if (this.currentRoute === stateName.trim()) {
          isActive = true;
        }
      }
    });
    return isActive;
  }

  public goToItem(routeName: string) {
    const url = `/usuario/${routeName}`;
    this.router.navigateByUrl(url);
  }

  private getCurrentPartial() {
    // TODO: Refatorar. Feito desta forma como workaround pra algum bug registrado.
    const parts = this.router.url.replace('/app/user/profile', '').split('/');
    this.currentRoute = parts.slice(-1)[0];
  }

}
