import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DonationModel } from 'src/app/core/models/product.model';
import { PaginationService } from 'src/app/core/services/pagination.service';
import { UserDataService } from 'src/app/core/services/user-data.service';
import { DonationStatus } from 'src/app/core/models/donation-status.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-donations',
  templateUrl: './donations.component.html',
  styleUrls: ['./donations.component.scss']
})
export class DonationsComponent implements OnInit, OnDestroy {
  donations: Observable<DonationModel>[] = []
  paymentEnum = DonationStatus

  constructor(
    public paginationService: PaginationService,
    private userData: UserDataService,
    private router: Router
  ) { }

  scrollHandler() {
    this.paginationService.more()
  }

  goToPix(donation: DonationModel) {
    this.router.navigate(['/usuario/pix', { qrCode: donation.qrCode, qrCodeImage: donation.qrCodeImage, expiracao: donation.expiracao }])
  }

  ngOnInit(): void {
    this.paginationService.init(`users/${this.userData.get().uid}/donations`, 'createDate', null, { reverse: true, prepend: false })
  }

  ngOnDestroy(): void {
    this.paginationService.clear()
  }

}
