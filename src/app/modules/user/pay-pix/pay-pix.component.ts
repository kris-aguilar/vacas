import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pay-pix',
  templateUrl: './pay-pix.component.html',
  styleUrls: ['./pay-pix.component.scss']
})
export class PayPixComponent implements OnInit {

  qrCode = ''
  qrCodeImage = ''
  expiracao = 0

  constructor(private route: ActivatedRoute, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.route.params.subscribe(async (params: Params) => {
      this.qrCode = params['qrCode']
      this.qrCodeImage = params['qrCodeImage']
      this.expiracao = params['expiracao']
    })
  }

  copiedText() {
    this.toastr.success('', 'Código copiado!')
  }

}
