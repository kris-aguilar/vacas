import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { DonationsComponent } from './donations/donations.component';
import { UserMainComponent } from './user-main/user-main.component';
import { LayoutModule } from 'src/app/layout/layout.module';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PayPixComponent } from './pay-pix/pay-pix.component';
import { ClipboardModule } from 'ngx-clipboard';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignDonosComponent } from './campaign-donos/campaign-donos.component';

@NgModule({
  declarations: [
    DonationsComponent,
    UserMainComponent,
    UserMenuComponent,
    PayPixComponent,
    CampaignListComponent,
    CampaignDonosComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    UserRoutingModule,
    InfiniteScrollModule,
    ClipboardModule
  ]
})
export class UserModule { }
