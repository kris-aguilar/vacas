import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignDonosComponent } from './campaign-donos/campaign-donos.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { DonationsComponent } from './donations/donations.component';
import { PayPixComponent } from './pay-pix/pay-pix.component';
import { UserMainComponent } from './user-main/user-main.component';

const routes: Routes = [{
  path: '',
  data: { title: 'Usuário' },
  component: UserMainComponent,
  children: [
    { path: '', redirectTo: 'doacoes', pathMatch: 'full' },
    { path: 'doacoes', component: DonationsComponent, data: { title: 'Minhas Doacões' } },
    { path: 'pix', component: PayPixComponent, data: { title: 'Pagar Pix' } },
    { path: 'campanhas/:id', component: CampaignDonosComponent, data: { title: 'Doações de Campanha' } },
    { path: 'campanhas', component: CampaignListComponent, data: { title: 'Minhas Campanhas' } },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
