import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CampaignStatus } from 'src/app/core/models/campaign-status.enum';
import { CampaignModel } from 'src/app/core/models/campaign.model';
import { PaginationService } from 'src/app/core/services/pagination.service';
import { UserDataService } from 'src/app/core/services/user-data.service';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss']
})
export class CampaignListComponent implements OnInit {

  campaignEnum = CampaignStatus

  constructor(
    public paginationService: PaginationService,
    private userData: UserDataService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.paginationService.init<CampaignModel>('campaigns', 'createDate', { 
      fieldPath: 'uid', 
      opStr: '==', 
      value: this.userData.get().uid }, 
      { reverse: true, prepend: false })
  }

  ngOnDestroy(): void {
    this.paginationService.clear()
  }

  scrollHandler() {
    this.paginationService.more()
  }

  goToDonos(campaignId: string) {
    this.router.navigate([`/usuario/campanhas/${campaignId}`])
  }

}
