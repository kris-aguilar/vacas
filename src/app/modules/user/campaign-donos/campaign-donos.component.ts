import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DonationModel } from 'src/app/core/models/product.model';
import { PaginationService } from 'src/app/core/services/pagination.service';

@Component({
  selector: 'app-campaign-donos',
  templateUrl: './campaign-donos.component.html',
  styleUrls: ['./campaign-donos.component.scss']
})
export class CampaignDonosComponent implements OnInit, OnDestroy {

  campaignId = ''

  constructor(
    private route: ActivatedRoute,
    public paginationService: PaginationService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(async (params: Params) => {
      this.campaignId = params['id']
      this.paginationService.init<DonationModel>('donations', 'createDate', {
        fieldPath: 'campaignId',
        opStr: '==',
        value: this.campaignId
      },
        { reverse: true, prepend: false, collectionGroup: true })
    })
  }

  ngOnDestroy(): void {
    this.paginationService.clear()
  }

}
