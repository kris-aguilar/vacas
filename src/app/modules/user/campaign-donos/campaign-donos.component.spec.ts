import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDonosComponent } from './campaign-donos.component';

describe('CampaignDonosComponent', () => {
  let component: CampaignDonosComponent;
  let fixture: ComponentFixture<CampaignDonosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignDonosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDonosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
